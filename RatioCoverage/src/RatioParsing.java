import static org.junit.Assert.*;

import org.junit.Test;

public class RatioParsing {

	@Test
	public void testPositivePositive () {
		assertEquals (new Ratio (1, 2), Ratio.parse("1/2"));
	}
	
	@Test
	public void testPositiveNegative () {
		assertEquals (new Ratio (1, -2), Ratio.parse ("1/-2"));
	}
	
	@Test
	public void testNegativePositive () {
		assertEquals (new Ratio (-1, 2), Ratio.parse("-1/2"));
	}
	
	@Test
	public void testNegativeNegative () {
		assertEquals (new Ratio (-1, -2), Ratio.parse("-1/-2"));
	}
	
	@Test
	public void testWhitespaces () {
		assertEquals (new Ratio (1, 2), Ratio.parse(" 1 / 2"));
	}
	
	@Test
	public void testBadFormatNum () {
		assertEquals (null, Ratio.parse ("1") );
	}
	
	@Test
	public void testBadFormatDen () {
		assertEquals (null, Ratio.parse ("/2"));
	}
	
	@Test
	public void testBadFormatNumNumDen () {
		assertEquals (null, Ratio.parse("1/2/3"));
		}
	
	@Test
	public void testNull () {
		assertEquals (null, Ratio.parse(null));
	}
	
	@Test
	public void testEmptyString () {
		assertEquals (null, Ratio.parse(""));
	}
	
	@Test
	public void testBadly () {
		assertEquals (null, Ratio.parse("Loool!!!!$%#*&"));
	}
}
