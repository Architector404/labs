import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class BranchCoverage {
	private Ratio x, y, z;
	@Before
	public void setUp() throws Exception {
		x = new Ratio (1, 1);
		y = new Ratio (2, 2);
		z = new Ratio (3, 3);
	}

	@Test
	public void tripleAndTest1 () {
		
		assertTrue (Ratio.tripleAnd(x, x, x));
	}
	
	@Test
	public void tripleAndTest2 () {
		assertFalse (Ratio.tripleAnd(x, y, z));
	}

}
