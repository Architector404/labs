
public class Parser {
	
	enum Token {
		Number,
		Plus,
		Minus,
		Mult,
		Div,
		EmptyString,
		Error;
		public Object
		value;
		}
	 
	public static Token ProcessLine(String line){
		if (line instanceof String == false)
			throw new IllegalArgumentException ();
		line.replace("\\s+", "");

		switch (line) {
		case "+":
		return Token.Plus;
		case "-":
		return Token.Minus;
		case "*":
		return Token.Mult;
		case "/":
		return Token.Div;
		case "":
		return Token.EmptyString;
		}
		 
		Ratio value = Ratio.parse(line);
		if (value != null) {
		Token num = Token.Number;
		num.value = value;
		return num;
		}
		return Token.Error;
		}
}
