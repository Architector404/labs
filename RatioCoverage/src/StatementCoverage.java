import static org.junit.Assert.*;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class StatementCoverage {
 
	@Test
	public void testPlus() {
	assertEquals (Parser.Token.Plus, Parser.ProcessLine("+"));
	}
	@Test
	public void testMinus () {
	assertEquals (Parser.Token.Minus, Parser.ProcessLine("-"));
	}
	@Test
	public void testMult () {
	assertEquals (Parser.Token.Mult, Parser.ProcessLine("*"));
	}
	@Test
	public void testDiv () {
	assertEquals (Parser.Token.Div, Parser.ProcessLine("/"));
	}
	@Test
	public void testEmpty () {
	assertEquals (Parser.Token.EmptyString, Parser.ProcessLine(""));
	}
	@Test
	public void testIncorrect () {
	assertEquals (Parser.Token.Error, Parser.ProcessLine("%*#!"));
	}
	@Test
	public void testRatio () {
	assertEquals (Parser.Token.Number, Parser.ProcessLine("1/2"));
	}
	
	@Rule
    public ExpectedException thrown= ExpectedException.none();
	
	@Test
	public void testNull () {
		thrown.expect (IllegalArgumentException.class);
		Parser.ProcessLine (null);
	}
}
