import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Ratio {
	private final int num;
	private final int den;
	/**
	* The class constructor initialized constant
	* @param numerator transmit the numerator
	* @param denominator transmit the denominator
	* @author Artyom Bogachuk 
	*/
	public Ratio(int numerator, int denominator){
		num = numerator;
		den = denominator;
	}
	/**
	* Returns a string of calculation results
	* @return num + "/" + den - scoring fraction 
	* @author Artyom Bogachuk
	*/
	 public String toString() {
	        
	        return num + "/" + den;
	    }
	
	public int num() {return num;}
	public int den() {return den;}
	/**
	* Calculates the sum of two rational fractions
	* @param y - term fraction
	* @return new Ratio(numerator, denominator) - initialization of scoring fractions
	* @author Artyom Bogachuk
	*/
	public Ratio plus(Ratio y)
	{
		Ratio x = this;
		int numerator = (x.num*y.den)+(x.den*y.num);
		int denominator = x.den * y.den;
		return new Ratio(numerator, denominator);
	}
	/**
	* It calculates the difference of rational fractions
	* @param y - subtracted fraction
	* @return new Ratio(numerator, denominator) - initialization of scoring fractions
	* @author Artyom Bogachuk
	*/
	public Ratio minus(Ratio y)
	{
		Ratio x = this;
		int numerator = (x.num*y.den)-(x.den*y.num);
		int denominator = x.den * y.den;
		return new Ratio(numerator, denominator);
	}
	/**
	* Calculates the product of rational fractions
	* @param y - fraction-factor
	* @return new Ratio(numerator, denominator) - initialization of scoring fractions
	* @author Artyom Bogachuk
	*/
	public Ratio times(Ratio y)
	{
		Ratio x = this;
		int numerator = x.num * y.num;
		int denominator = x.den * y.den;
		return new Ratio(numerator, denominator);
	}
	/**
	* Calculates the quotient of two rational fractions
	* @param y - divider
	* @return new Ratio(numerator, denominator) - initialization of scoring fractions
	* @author Artyom Bogachuk
	*/
	public Ratio divides(Ratio y)
	{
		Ratio x = this;
		int numerator = x.num * y.den;
		int denominator = x.den * y.num;
		return new Ratio(numerator, denominator);
	}
	/**
	* Compares rational fractions
	* @param y - second fraction compared
	* @return less if the first fraction is less than a second 
	* equal if the fractions is equal 
	* larger if the first fraction more than the second
	* @author Artyom Bogachuk
	*/
	public String order(Ratio y)
	{
		Ratio x = this;
		if ((x.num * y.den)<(x.den * y.num))
		{
		String less = " < ";
		return less;
		}
		if ((x.num * y.den)==(x.den * y.num))
		{
		String equal = " = ";
		return equal;}
		else {String larger = " > "; return larger;}
	}
	/**
	* Compare with other object.
	* @param obj the object to compare
	* @return true if objects are equal and false for else
	* @author Artyom Bogachuk
	*/
	@Override
	public boolean equals (Object obj) {
	if (obj instanceof Ratio)
	if ( (num == ((Ratio)obj).num()) &&
	(den == ((Ratio)obj).den()))
	return true;
	return false;
	}
	public int hashCode () {
	return Double.valueOf(num).hashCode() ^ Double.valueOf(den).hashCode();
	}
	/**
	* Triple And.
	* @author Artyom Bogachuk
	*/ 
	 public static boolean tripleAnd (Ratio a, Ratio b, Ratio c) {
	    	if ((a == b) && (b == c) && (a == c))
	    		return true;
	    	return false;
	    }
	/**
	* Parse.
	* @author Artyom Bogachuk
	*/ 
	public static Ratio parse(String input) {
    	if (input instanceof String == false)
    		return null;
    	input = input.replace(" ", ""); 
    	String number = "([+-]?\\d+\\.?\\d*)";
    	String ratio = number + "/" + number;
    	Pattern pattern = Pattern.compile (ratio, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(input);
		if (matcher.matches()) {
			int numerator = Integer.parseInt(matcher.group(1));
			int denominator = Integer.parseInt(matcher.group(2));
			return new Ratio (numerator, denominator);
		}
		return null;
    }
	/**
	* Creating objects x and y. Displaying calculation results.
	* @author Artyom Bogachuk
	*/
	public static void main(String[] args) {
	Ratio x = new Ratio(5,6);
	Ratio y = new Ratio(3,4);
	
	System.out.println("x                 = " + x);
	System.out.println("y                 = " + y);
	System.out.println("Numerator(x)      = " + x.num());
    System.out.println("Denominator(x)    = " + x.den());
    System.out.println("Numerator(y)      = " + y.num());
    System.out.println("Denominator(y)    = " + y.den());
	System.out.println("Addition          = " + x.plus(y));
	System.out.println("Subtraction       = " + x.minus(y));
	System.out.println("Multiplication    = " + x.times(y));
	System.out.println("Division          = " + x.divides(y));
	System.out.println("Ordering          = " + x + x.order(y) + y);
	}
}
