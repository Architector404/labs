/**
* @author Artyom Bogachuk
*/
import static org.junit.Assert.*;

import org.junit.Test;

public class RatioTest {

	@Test
	public void eqalsReflexive () {
	Ratio x = new Ratio (1, -1);
	assertTrue (x.equals(x));
	}
	@Test
	public void equalsSymmetric () {
	Ratio x = new Ratio (1, -1);
	Ratio y = new Ratio (1, -1);
	assertTrue (x.equals(y));
	assertTrue (y.equals(x));
	}
	@Test
	public void equalsTransitivity () {
	Ratio x = new Ratio (1, -1);
	Ratio y = new Ratio (1, -1);
	Ratio z = new Ratio (1, -1);
	assertTrue (x.equals(y));
	assertTrue (y.equals(z));
	assertTrue (x.equals(z));
	}
	@Test
	public void equalsNull () {
	Ratio x = new Ratio (1, -1);
	assertFalse (x.equals(null));
	}
	
	@Test
	public void equalsNotEqual () {
	Ratio x = new Ratio (1, -1);
	Ratio y = new Ratio (0, 0);
	assertFalse ("(1/-1) must not be equal to (0/0)", x.equals(y));
	}
	@Test
	public void testPlus() {
		Ratio x = new Ratio(3,4);
		Ratio y = new Ratio(5,6);
		Ratio z = new Ratio(38,24);
		assertEquals("(3/4)+(5/6) must be 38/24", z, x.plus(y));
	}
	@Test
	public void testMinus() {
		Ratio x = new Ratio(3,4);
		Ratio y = new Ratio(5,6);
		Ratio z = new Ratio(-2,24);
		assertEquals("(3/4)-(5/6) must be -2/24", z, x.minus(y));
	}
	@Test
	public void testTimes() {
		Ratio x = new Ratio(3,4);
		Ratio y = new Ratio(5,6);
		Ratio z = new Ratio(15,24);
		assertEquals("(3/4)*(5/6) must be 15/24", z, x.times(y));
	}
	@Test
	public void testDivides() {
		Ratio x = new Ratio(3,4);
		Ratio y = new Ratio(5,6);
		Ratio z = new Ratio(18,20);
		assertEquals("(3/4):(5/6) must be 18/20", z, x.divides(y));
	}

}
