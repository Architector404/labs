public class Ratio {
	private final int num;
	private final int den;
	/**
	* ����������� ������, ���������������� ���������
	* @param numerator ������� �������� ���������
	* @param denominator ������� �������� �����������
	* @author ���� �������
	*/
	public Ratio(int numerator, int denominator){
		num = numerator;
		den = denominator;
	}
	/**
	* ���������� ��������� �������� ����������� ����������
	* @return num + "/" + den - �������������� ����� 
	* @author ���� �������
	*/
	 public String toString() {
	        
	        return num + "/" + den;
	    }
	
	public int num() {return num;}
	public int den() {return den;}
	/**
	* ��������� ����� ���� ������������ ������
	* @param y - ��������� �����
	* @return new Ratio(numerator, denominator) - ������������� �������������� �����
	* @author ���� �������
	*/
	public Ratio plus(Ratio y)
	{
		Ratio x = this;
		int numerator = (x.num*y.den)+(x.den*y.num);
		int denominator = x.den * y.den;
		return new Ratio(numerator, denominator);
	}
	/**
	* ��������� �������� ���� ������������ ������
	* @param y - ���������� �����
	* @return new Ratio(numerator, denominator) - ������������� �������������� �����
	* @author ���� �������
	*/
	public Ratio minus(Ratio y)
	{
		Ratio x = this;
		int numerator = (x.num*y.den)-(x.den*y.num);
		int denominator = x.den * y.den;
		return new Ratio(numerator, denominator);
	}
	/**
	* ��������� ������������ ���� ������������ ������
	* @param y - �����-���������
	* @return new Ratio(numerator, denominator) - ������������� �������������� �����
	* @author ���� �������
	*/
	public Ratio times(Ratio y)
	{
		Ratio x = this;
		int numerator = x.num * y.num;
		int denominator = x.den * y.den;
		return new Ratio(numerator, denominator);
	}
	/**
	* ��������� ������� ���� ������������ ������
	* @param y - ��������
	* @return new Ratio(numerator, denominator) - ������������� �������������� �����
	* @author ���� �������
	*/
	public Ratio divides(Ratio y)
	{
		Ratio x = this;
		int numerator = x.num * y.den;
		int denominator = x.den * y.num;
		return new Ratio(numerator, denominator);
	}
	/**
	* ���������� ������������ �����
	* @param y - ������ ����� � ��������� 
	* @return less ���� ������ ����� ������ ������ equal ���� ����� ����� larger ���� ������ ����� ������ ������
	* @author ���� �������
	*/
	public String order(Ratio y)
	{
		Ratio x = this;
		if ((x.num * y.den)<(x.den * y.num))
		{
		String less = " < ";
		return less;
		}
		if ((x.num * y.den)==(x.den * y.num))
		{
		String equal = " = ";
		return equal;}
		else {String larger = " > "; return larger;}
	}
	/**
	* ��������� ������� � �������.
	* @param obj ������ ��� ���������
	* @return true ���� ������� ������������ � false � ��������� ������
	* @author ���� �������
	*/
	@Override
	public boolean equals (Object obj) {
	if (obj instanceof Ratio)
	if ( (num == ((Ratio)obj).num()) &&
	(den == ((Ratio)obj).den()))
	return true;
	return false;
	}
	public int hashCode () {
	return Double.valueOf(num).hashCode() ^ Double.valueOf(den).hashCode();
	}
	/**
	* �������� �������� � � �. ����� �� ����� ����������� ����������.
	* @author ���� �������
	*/
	public static void main(String[] args) {
	Ratio x = new Ratio(5,6);
	Ratio y = new Ratio(3,4);
	
	System.out.println("x                 = " + x);
	System.out.println("y                 = " + y);
	System.out.println("Numerator(x)      = " + x.num());
    System.out.println("Denominator(x)    = " + x.den());
    System.out.println("Numerator(y)      = " + y.num());
    System.out.println("Denominator(y)    = " + y.den());
	System.out.println("Addition          = " + x.plus(y));
	System.out.println("Subtraction       = " + x.minus(y));
	System.out.println("Multiplication    = " + x.times(y));
	System.out.println("Division          = " + x.divides(y));
	System.out.println("Ordering          = " + x + x.order(y) + y);
	}
}
