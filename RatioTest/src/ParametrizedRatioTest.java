/**
* @author Artyom Bogachuk
*/
import static org.junit.Assert.*;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.junit.runner.RunWith;

@RunWith(Parameterized.class)
public class ParametrizedRatioTest {

	@Parameters
	public static Collection<Object[]> data() {
	return Arrays.asList(new Object [][] {
	{ new Ratio(1, 2), new Ratio(2, 3), new Ratio (2, 6) } ,
	{ new Ratio(7, 8), new Ratio(3, 4), new Ratio (21, 32) } ,
	{ new Ratio(6, 7), new Ratio(-1, -2), new Ratio (-6, -14) }
	});
	}
	private Ratio x;
	private Ratio y;
	private Ratio z;
	public ParametrizedRatioTest (Ratio a, Ratio b, Ratio c) {
	x = a;
	y = b;
	z = c;
	}
	@Test
	public void testTimes() {
		assertEquals(z, x.times(y));
	}

}
