/**
* @author Artyom Bogachuk
*/
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class FixtureRatioTest {

	private Ratio x;
	
	@Before
	public void setUp() throws Exception {
	x = new Ratio(1,2);
	}

	@After
	public void tearDown() throws Exception {
	
	}

	@Test
	public void testPlus() {
		Ratio y = new Ratio (1, 2);
		Ratio z = new Ratio (4, 4);
		assertEquals ("(1/2)+(1/2) must be (4/4)", z, x.plus(y));
	}
	@Test
	public void testMinus() {
		Ratio y = new Ratio (1, 2);
		Ratio z = new Ratio (0, 0);
		assertEquals ("(1/2)-(1/2) must be (0)", z, x.minus(y));
	}
	@Test
	public void testTimes() {
		Ratio y = new Ratio (1, 2);
		Ratio z = new Ratio (1, 4);
		assertEquals ("(1/2)*(1/2) must be (1/4)", z, x.times(y));
	}
	@Test
	public void testDivides() {
		Ratio y = new Ratio (1, 2);
		Ratio z = new Ratio (2, 2);
		assertEquals ("(1/2):(1/2) must be (2/2)", z, x.divides(y));
	}
	
}
