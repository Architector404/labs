package logic;

import java.util.Observable;

public class OrderingEventSource extends Observable{
	public OrderingEventSource(iCore target) {
		addObserver(target);
	}
	
	public void SendOrder(Object data) {
		setChanged();
		notifyObservers(data);
	}
}
