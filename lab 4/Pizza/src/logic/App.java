package logic;

import javax.swing.JOptionPane;

import gui.OrderingDialog;

public class App {
	final static iCore core = new Core();
	public static void main(String[] args) {
		try {
			new OrderingDialog(core).start();
		}
		catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Error occured! See stack trace.", "Error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
		
	}
}
