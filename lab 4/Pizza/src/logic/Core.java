package logic;

import java.util.Observable;

import javax.swing.JOptionPane;

public class Core implements iCore{

	@Override
	public final void update(Observable event_source, Object data) {
		if (event_source instanceof OrderingEventSource) {
			Order received = (Order)data;
			if (data instanceof Order)
				JOptionPane.showMessageDialog(null, received.toString(), "Info", JOptionPane.INFORMATION_MESSAGE);
			else if (data == null)
				JOptionPane.showMessageDialog(null, "Incorrect order format", "Error", JOptionPane.WARNING_MESSAGE);
		}
		else
			JOptionPane.showMessageDialog(null, "Unknown event source");
	}
}
