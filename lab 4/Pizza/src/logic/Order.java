package logic;

import java.text.ParseException;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public final class Order {
public Order () {}
	
	public Order (String adress, String time, String name, String size, String sauce, String drink) 
			throws IllegalArgumentException{
		adress_ = adress;
		name_ = name;
		size_ = size;
		sauce_ = sauce;
		drink_ = drink;
		try {
		time_ = DeliveryTime.parse(time);
		}
		catch (ParseException e) {
			throw new IllegalArgumentException();
		}
	}
	
	public String adress_ = new String (),
			name_ = new String(),
			size_ = new String(),
			sauce_ = new String(),
			drink_ = new String();
	public DeliveryTime time_ = new DeliveryTime();
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) { return false; }
		if (obj == this) { return true; }
		if (obj.getClass() != getClass()) {
			return false;
		}
		Order rhs = (Order) obj;
		return new EqualsBuilder()
				.append(adress_, rhs.adress_)
				.append(time_, rhs.time_)
				.append(name_, rhs.name_)
				.append(size_, rhs.size_)
				.append(sauce_, rhs.sauce_)
				.append(drink_, rhs.drink_)
				.isEquals();
	}
	
	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(adress_)
				.append(time_)
				.append(name_)
				.append(size_)
				.append(sauce_)
				.append(drink_)
				.toHashCode();
	}
	
	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append(adress_)
				.append(time_)
				.append(name_)
				.append(size_)
				.append(sauce_)
				.append(drink_)
				.toString();
	}
}