package logic;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public final class DeliveryTime {
	public static final String SEPARATOR = ":";
	private Date date_;
	
	public DeliveryTime() {
		date_ = new Date();
	}
	
	public DeliveryTime(Date date) {
		date_ = date;
	}
	
	private String Format(int i) {
		String res = Integer.toString(i);
		if (res.length() < 2)
			return "0" + res;
		else
			return res.substring(res.length() - 2);
	}
	
	@Override
	public String toString() {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date_);
		
		String 	hour = Format(cal.get(Calendar.HOUR_OF_DAY)),
				minute = Format(cal.get(Calendar.MINUTE));			 
		return hour + SEPARATOR + minute;
	}
	
	public static DeliveryTime parse(String date_str) throws ParseException {
		SimpleDateFormat date_format = new SimpleDateFormat("HH:mm");
		Date date = date_format.parse(date_str);
		return new DeliveryTime(date);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) { return false; }
		if (obj == this) { return true; }
		if (obj.getClass() != getClass()) {
			return false;
		}
		DeliveryTime rhs = (DeliveryTime) obj;
		return new EqualsBuilder()
				.append(date_, rhs.date_)
				.isEquals();
	}
	
	@Override
	public int hashCode () {
		return new HashCodeBuilder(17, 37)
				.append(date_)
				.toHashCode();
	}
}
