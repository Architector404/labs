package logic;

import java.util.Observable;
import java.util.Observer;

public interface iCore extends Observer{
	@Override
	abstract public void update(Observable event_source, Object data);
}
