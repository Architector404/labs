package test;

import static org.junit.Assert.*;

import org.junit.Test;

import logic.Order;

public class OrderTest {

	@Test
	public void basic_validation() {
		Order expected = new Order("���������� 26�", "20:30", "�������", "125�", "�����", "J7");
		Order actual = new Order("���������� 26�", "20:30", "�������", "125�", "�����", "J7");
		assertEquals(expected, actual);
	}

	@Test
	public void parsing_validation() {
		String 	adr = "���������� 26�",tm = "20:30", 
				nm = "�������", sz = "125�", 
				sc = "�����", dr = "J7";
		Order order = new Order(adr, tm, nm, sz, sc, dr);
		
		assertEquals(adr, order.adress_.toString());
		assertEquals(tm, order.time_.toString());
		assertEquals(nm, order.name_.toString());
		assertEquals(sz, order.size_.toString());
		assertEquals(sc, order.sauce_.toString());
		assertEquals(dr, order.drink_.toString());
	}

}
