package test;

import java.util.Observable;
import java.util.concurrent.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import logic.iCore;
import logic.Order;

public class CoreStub implements iCore{
	private static final int COUNT = 1;
	private CountDownLatch order_latch_ = new CountDownLatch(COUNT);
	private Order order_itself_ = new Order();
	public final static Lock lock = new ReentrantLock();
	
	public Order GetOrder() {
		return order_itself_;
	}
	
	public void Reset() {
		order_latch_ = new CountDownLatch(COUNT);
		order_itself_ = null;
	}
	
	public void Await() {
		try {
			order_latch_.await();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public final void update(Observable event_source, Object data) {
		order_itself_ = (Order)data;
		Free();
	}

	public void Free() {
		order_latch_.countDown();
	}
}
