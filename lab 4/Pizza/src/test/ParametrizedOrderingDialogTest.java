package test;

import static org.fest.assertions.Assertions.assertThat;
import static org.fest.swing.edt.GuiActionRunner.execute;
import static org.junit.Assert.*;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.fest.swing.annotation.RunsInEDT;
import org.fest.swing.edt.GuiQuery;
import org.fest.swing.fixture.FrameFixture;
import org.fest.swing.fixture.JComboBoxFixture;
import org.fest.swing.fixture.JTextComponentFixture;
import org.fest.swing.junit.testcase.FestSwingJUnitTestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

import gui.OrderingDialog;
import logic.Order;

@RunWith(Parameterized.class)
public final class ParametrizedOrderingDialogTest extends FestSwingJUnitTestCase {
	private FrameFixture dialog_;
	private static CoreStub coreStub_ = new CoreStub();
	private OrderStub request_;
	private OrderStub expected_;
	
	@Override
	protected void onSetUp() {
		dialog_ = new FrameFixture(robot(), runOrderDialogue());
		dialog_.show();
	}
	
	@RunsInEDT
	private static OrderingDialog runOrderDialogue() {
		return execute(new GuiQuery<OrderingDialog>() {
			protected OrderingDialog executeInEDT() {
				return new OrderingDialog(coreStub_);
			}
		});		
	}
	
	@Parameters
	public static Collection<Object[]> data() {	
		OrderStub ordinary = new OrderStub("���������� 26�", "20:30", "�������", "125�", "�����", "J7");
		return Arrays.asList(new Object[][] {
			{ordinary, ordinary},
			{new OrderStub("","","","","",""), null}
		});
	}
	
	public ParametrizedOrderingDialogTest(OrderStub request, OrderStub expected) {
		request_ = request;
		expected_ = expected;
	}
	
	@Test 
	public void test() {
		coreStub_.Reset();
		{
			// TextBoxes
			visitTextBox(OrderingDialog.TF_ADRESS, request_.adr_);
			visitTextBox(OrderingDialog.TF_TIME, request_.tm_);
			// ComboBoxes
			visitComboBox(OrderingDialog.CMB_NAME, request_.nm_);
			visitComboBox(OrderingDialog.CMB_SIZE, request_.sz_);
			visitComboBox(OrderingDialog.CMB_SAUCE, request_.sc_);
			visitComboBox(OrderingDialog.CMB_DRINK, request_.dr_);
			// Click the button
			dialog_.button(OrderingDialog.BTN_SEND).click();
		}
		coreStub_.Await();
		
		OrderStub actual = OrderStub.FromOrder(coreStub_.GetOrder());
		assertEquals(expected_, actual);
	}

	public void visitTextBox(String name, String text) {
		JTextComponentFixture the_box = dialog_.textBox(name);
		the_box.setText(text);
		assertThat(the_box.text()).contains(text);
	}
	
	public void visitComboBox(String name, String text) {
		if (text.isEmpty())
			return;
		JComboBoxFixture the_box = dialog_.comboBox(name);
		the_box.selectItem(text);
		int idx = Arrays.asList(the_box.contents()).indexOf(text);
		assertThat(the_box.valueAt(idx)).isEqualTo(text);
	}
}

final class OrderStub {
	public String adr_, tm_, nm_, sz_, sc_, dr_;
	
	public static OrderStub FromOrder(Order order) {
		if (order == null) { return null; }		
		OrderStub stub = new OrderStub();
		stub.adr_ = order.adress_.toString();
		stub.tm_ = order.time_.toString();
		stub.nm_ = order.name_.toString();
		stub.sz_ = order.size_.toString();
		stub.sc_ = order.sauce_.toString();
		stub.dr_ = order.drink_.toString();
		return stub;
	}
	
	public OrderStub(String adr, String tm, String nm, String sz, String sc, String dr) {
		adr_ = adr; tm_ = tm;
		nm_ = nm; sz_ = sz;
		sc_ = sc; dr_ = dr;
	}
	
	public OrderStub() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) { return false; }
		if (obj == this) { return true; }
		if (obj.getClass() != getClass()) { return false; }
		OrderStub order = (OrderStub)obj;	
		return new EqualsBuilder()
			.append(adr_, order.adr_)
			.append(tm_, order.tm_)
			.append(nm_, order.nm_)
			.append(sz_, order.sz_)
			.append(sc_, order.sc_)
			.append(dr_, order.dr_)
			.isEquals();
	}
	
	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(adr_)
				.append(tm_)
				.append(nm_)
				.append(sz_)
				.append(sc_)
				.append(dr_)
				.toHashCode();
	}
	
	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append(adr_)
				.append(tm_)
				.append(nm_)
				.append(sz_)
				.append(sc_)
				.append(dr_)
				.toString();
	}
}










