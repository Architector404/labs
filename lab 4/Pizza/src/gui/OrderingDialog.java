package gui;

import java.awt.EventQueue;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

//import logic.Comfort;
import logic.iCore;
import logic.Order;
import logic.OrderingEventSource;

public class OrderingDialog extends JFrame {
	private JPanel contentPane;
	private JTextField tfAdress;
	private JTextField tfTime;
	
	public static final String TF_ADRESS = "tfAdress";
	public static final String TF_TIME = "tfTime";
	public static final String CMB_NAME = "cmbName";
	public static final String CMB_SIZE = "cmbSize";
	public static final String CMB_SAUCE = "cmbSauce";
	public static final String CMB_DRINK = "cmbDrink";
	public static final String BTN_SEND = "btnSend";
	
	public void start() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					//OrderingDialogue frame = new OrderingDialogue(app);
					setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();				
				}
			}
		});
	}
	
	public OrderingDialog(iCore app) {
		final OrderingEventSource event_src = new OrderingEventSource(app);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 388);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblAdress = new JLabel("�����");
		lblAdress.setBounds(10, 37, 86, 14);
		getContentPane().add(lblAdress);
		
		tfAdress = new JTextField();
		tfAdress.setBounds(73, 34, 142, 20);
		getContentPane().add(tfAdress);
		tfAdress.setColumns(10);
		
		JLabel lblTime = new JLabel("�����");
		lblTime.setBounds(10, 65, 86, 14);
		getContentPane().add(lblTime);
		
		tfTime = new JTextField();
		tfTime.setBounds(73, 62, 53, 20);
		getContentPane().add(tfTime);
		tfTime.setColumns(10);
		
		JLabel lblName = new JLabel("�������� �����");
		lblName.setBounds(10, 137, 99, 14);
		getContentPane().add(lblName);
		
		JComboBox cmbName = new JComboBox();
		cmbName.setBounds(114, 134, 96, 20);
		cmbName.setModel(new DefaultComboBoxModel(new String[] {"�������", "���������", "������", "��������", "���������", "��������������"}));
		cmbName.setSelectedItem("1");
		getContentPane().add(cmbName);
		
		JLabel lblSize = new JLabel("������ ������");
		lblSize.setBounds(220, 137, 92, 14);
		getContentPane().add(lblSize);
		
		JComboBox cmbSize = new JComboBox();
		cmbSize.setBounds(322, 134, 79, 20);
		cmbSize.setModel(new DefaultComboBoxModel(new String[] {"100�", "125�", "187�"}));
		cmbSize.setSelectedItem("0");
		getContentPane().add(cmbSize);
		
		JLabel lblSauce = new JLabel("����");
		lblSauce.setBounds(66, 184, 30, 14);
		getContentPane().add(lblSauce);
		
		JComboBox cmbSauce = new JComboBox();
		cmbSauce.setBounds(114, 181, 96, 20);
		cmbSauce.setModel(new DefaultComboBoxModel(new String[] {"��������", "�����", "������"}));
		cmbSauce.setSelectedItem("1");
		getContentPane().add(cmbSauce);
		
		JLabel lblDrink = new JLabel("�������");
		lblDrink.setBounds(255, 184, 53, 14);
		getContentPane().add(lblDrink);
		
		JComboBox cmbDrink = new JComboBox();
		cmbDrink.setBounds(322, 181, 79, 20);
		cmbDrink.setModel(new DefaultComboBoxModel(new String[] {"Coca-Cola", "Pepsi", "J7"}));
		cmbDrink.setSelectedItem("2");
		getContentPane().add(cmbDrink);
		
		JButton btnSend = new JButton("���������");
		btnSend.setBounds(161, 280, 104, 30);
		getContentPane().add(btnSend);
		
		btnSend.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
				Order order = new Order(
						tfAdress.getText(),
						tfTime.getText(),
						(String)cmbName.getSelectedItem(),
						(String)cmbSize.getSelectedItem(),
						(String)cmbSauce.getSelectedItem(),
						(String)cmbDrink.getSelectedItem()
						);
				event_src.SendOrder(order);
				}
				catch (IllegalArgumentException ex) {
					event_src.SendOrder(null);
				}
			}	
		});
	}
}
